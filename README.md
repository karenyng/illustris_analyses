# Repo for Illustris analyses and paper  
https://trello.com/b/P23i2y7g/collaboration-illustris-simulation

# Plots
[IPython
notebook](http://nbviewer.ipython.org/urls/bitbucket.org/karenyng/illustris_analyses/raw/d1aebf98fbe273e9bb750d9c829904fce9cb0495/code/analyses/visualize_Illustris_peaks_and_density.ipynb)
of galaxy luminosity maps with associated peaks.

[IPython
notebook](http://nbviewer.ipython.org/urls/bitbucket.org/karenyng/illustris_analyses/raw/afb3f1a40ac117e025218a48da34a22d7b4b7863/code/prototypes/Informed_peak_finding.ipynb)
of DM particle density histograms with associated peaks.
Since this notebook is quite large, you may need to refresh the website a few
times for it to be rendered correctly.


# Use 
If you use any part of this repo, please cite Ng et al. (in prep)


# prerequisites 
* see `/code/package_version.txt`

To install the package prerequisites, use `pip`:
```bash
$ pip install -r /code/package_version.txt
```
You are advised to install the package prerequisites to a python virtual environment.


